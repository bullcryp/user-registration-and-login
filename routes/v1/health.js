import express from 'express';

import { SERVICE_NAME, SERVICE_VERSION } from '../../consts.js';

const router = express.Router();

router.get('/', async (req, res) => {
  res.send({ name: `${SERVICE_NAME} ${SERVICE_VERSION}`, status: 'OK' });
});

export default router;
