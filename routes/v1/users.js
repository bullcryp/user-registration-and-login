import express from 'express';
import httpStatusCodes from 'http-status-codes';
import { errors } from '@test/library-logging';

import userService from '../../services/user-service.js';
import emailVerificationService from '../../services/email-verification-service.js';
import sessionService from '../../services/session-service.js';
import emailService from '../../services/email-service.js';
import { ONE_MONTH_SECONDS, ONE_WEEK_SECONDS } from '../../consts.js';

const router = express.Router();

router.post('/register', async (req, res, next) => {
  const {
    password, email, termsAccepted = false, rememberMe = false,
  } = req.body;

  if (!termsAccepted) {
    return next(new errors.UnprocessableEntity('Terms and conditions were not accepted.'));
  }

  let newUser;
  try {
    newUser = await userService.createUser(email, password, termsAccepted);
  } catch (err) {
    return next(err);
  }

  let token;

  try {
    token = await sessionService.createSession(
      newUser.userId, rememberMe ? ONE_MONTH_SECONDS : ONE_WEEK_SECONDS,
    );
  } catch (err) {
    return next(errors.InternalServerError.logAndConvert(err, 'Failed to generate token.'));
  }

  try {
    const emailVerificationToken = await emailVerificationService.createToken(newUser.userId);
    await emailService.sendVerificationEmail(newUser.email, emailVerificationToken);
  } catch (err) {
    // This is not a fatal error so we just log it and continue executing.
    errors.FailedDependency.logAndConvert(err, 'Failed to send verification e-mail.');
  }

  return res.send({ token, userId: newUser.userId });
});

router.patch('/verification-status', async (req, res, next) => {
  let userId;

  try {
    userId = await emailVerificationService.validateToken(req.body.token);
  } catch (error) {
    return next(errors.BadRequest.logAndConvert(error, 'Failed to verify token.'));
  }

  if (userId === undefined) {
    return next(new errors.NotFound('Failed to find associated verification request.'));
  }

  let verifiedUser;

  try {
    verifiedUser = await userService.verifyUser(userId);
  } catch (error) {
    return next(error);
  }

  try {
    userId = await emailVerificationService.deleteToken(req.body.token);
  } catch (error) {
    // This is not a fatal error so we just log it and continue executing.
    errors.BadRequest.logAndConvert(error, 'Failed to verify token.');
  }

  return res.send({ userId: verifiedUser.userId });
});

router.get('/session/:token', async (req, res, next) => {
  try {
    const session = await sessionService.getSession(req.params.token);
    if (session !== undefined) {
      return res.sendStatus(httpStatusCodes.OK);
    }
  } catch (error) {
    next(error);
  }

  return next(new errors.UnauthorizedRequest('The session is invalid.'));
});

router.post('/session', async (req, res, next) => {
  const { email, password, rememberMe } = req.body;
  let user;
  try {
    user = await userService.authenticateUser(email, password);
    if (!user) return next(new errors.UnauthorizedRequest('Login failed.'));
  } catch (error) {
    return next(error);
  }

  let token;
  try {
    token = await sessionService.createSession(
      user.userId, rememberMe ? ONE_MONTH_SECONDS : ONE_WEEK_SECONDS,
    );
  } catch (error) {
    return next(error);
  }

  return res.send({ token, userId: user.userId });
});

router.delete('/session', async (req, res, next) => {
  try {
    const session = await sessionService.deleteSession(req.body.token);
    if (session !== undefined) {
      return res.sendStatus(httpStatusCodes.OK);
    }
  } catch (error) {
    next(error);
  }

  return next(new errors.BadRequest('Failed to delete session.'));
});

router.patch('/active-session', async (req, res, next) => {
  try {
    const session = await sessionService.makeActive(req.body.token);
    if (session !== undefined) {
      return res.sendStatus(httpStatusCodes.OK);
    }
  } catch (error) {
    return next(error);
  }

  return next(new errors.BadRequest('Failed to change active session.'));
});

export default router;
