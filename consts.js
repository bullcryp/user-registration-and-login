import packageJson from './package.json';

// service details
export const SERVICE_NAME = packageJson.name;
export const SERVICE_VERSION = packageJson.version;

// add constants to be used across the micro-service here.
export const ONE_MONTH_SECONDS = 2.628e+6;
export const ONE_WEEK_SECONDS = 604800;
