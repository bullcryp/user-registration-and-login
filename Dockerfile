# use latest version of node
FROM node:12

ARG ENVIRONMENT=prod

# npm token to be used for private repository
ARG NPM_TOKEN

# service specific environment variables (always make sure this syncs up with the convig/custom-environment-variables.json)
ARG CERBERUS_DB_PORT
ARG CERBERUS_DB
ARG CERBERUS_DB_HOST
ARG CERBERUS_DB_DIALECT
ARG CERBERUS_DB_PASSWORD
ARG CERBERUS_DB_USERNAME
ARG CERBERUS_LOCATION_FOR_HERMES
ARG CERBERUS_API_VERSION_FOR_HERMES
ARG CERBERUS_JWT_ISSUER
ARG CERBERUS_JWT_SUBJECT
ARG CERBERUS_JWT_AUDIENCE
ARG CERBERUS_JWT_ALGORITHM
ARG CERBERUS_JWT_PUBLIC_KEY
ARG CERBERUS_JWT_PRIVATE_KEY
ARG CERBERUS_REDIS_HOST
ARG CERBERUS_REDIS_PORT

# map args to environment variables
ENV CERBERUS_DB_PORT $CERBERUS_DB_PORT
ENV CERBERUS_DB $CERBERUS_DB
ENV CERBERUS_DB_HOST $CERBERUS_DB_HOST
ENV CERBERUS_DB_DIALECT $CERBERUS_DB_DIALECT
ENV CERBERUS_DB_PASSWORD $CERBERUS_DB_PASSWORD
ENV CERBERUS_DB_USERNAME $CERBERUS_DB_USERNAME
ENV CERBERUS_LOCATION_FOR_HERMES $CERBERUS_LOCATION_FOR_HERMES
ENV CERBERUS_API_VERSION_FOR_HERMES $CERBERUS_API_VERSION_FOR_HERMES
ENV CERBERUS_JWT_ISSUER $CERBERUS_JWT_ISSUER
ENV CERBERUS_JWT_SUBJECT $CERBERUS_JWT_SUBJECT
ENV CERBERUS_JWT_AUDIENCE $CERBERUS_JWT_AUDIENCE
ENV CERBERUS_JWT_ALGORITHM $CERBERUS_JWT_ALGORITHM
ENV CERBERUS_JWT_PUBLIC_KEY $CERBERUS_JWT_PUBLIC_KEY
ENV CERBERUS_JWT_PRIVATE_KEY $CERBERUS_JWT_PRIVATE_KEY
ENV CERBERUS_REDIS_HOST $CERBERUS_REDIS_HOST
ENV CERBERUS_REDIS_PORT $CERBERUS_REDIS_PORT

# set working directory
WORKDIR /dist/

# first install node modules to help with Docker chaching
COPY package*.json ./

# determine whether we need to install dev dependencies or not
RUN if [ ${ENVIRONMENT} = "dev" ] ; then \
      # we have to avoid exposing our token in one of the layers
      echo "//registry.npmjs.org/:_authToken=$NPM_TOKEN" > .npmrc && \
      npm install && \
      rm -f .npmrc; \
    else \
      # we have to avoid exposing our token in one of the layers
      echo "//registry.npmjs.org/:_authToken=$NPM_TOKEN" > .npmrc && \
      npm ci --only=production && \
      rm -f .npmrc; \
    fi

# Copy source code
COPY . .

# run tests if we are in development environment
RUN if [ ${ENVIRONMENT} = "dev" ] ; then \
      npm test; \
    fi

# expose port 3000 & start app
EXPOSE 3000
CMD ["npm", "start"]
