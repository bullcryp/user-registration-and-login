/**
 * @description Encodes a key value pair as a base64 token in the format ${key}::${value}
 *
 * @param {string} key The key for the token (normally the user ID)
 * @param {string} value The value for the token (normally the session ID)
 * @returns {string} Returns a base64 encoded token.
 */
export const encodeToken = (key, value) => {
  const asciiToken = `${key}::${value}`;
  const base64Token = Buffer.from(asciiToken).toString('base64');
  return base64Token;
};

/**
 * @description Decodes a base64 token to an ASCII string and separates it
 * into its key-value pair.
 *
 * @param {string} base64Token The base64 encoded token to be decoded.
 * @returns {object} Returns an object containing the key-value pair.
 */
export const decodeToken = (base64Token) => {
  const asciiToken = Buffer.from(base64Token, 'base64').toString('ascii');
  const tokenRegex = /^([a-z A-Z 0-9 -]+)::([a-z A-Z 0-9 -]+)$/g;
  const results = tokenRegex.exec(asciiToken);

  if (results === null) {
    return undefined;
  }

  return {
    key: results[1],
    value: results[2],
  };
};

export default {
  encodeToken,
  decodeToken,
};
