import IoRedis from 'ioredis';
import uuid from 'uuid';
import moment from 'moment';

import { encodeToken, decodeToken } from '../utils.js';

const SESSION_NAMESPACE = userId => `users:${userId}:sessions:`;

class Session {
  constructor(id, active, expiresIn) {
    this.id = id;
    this.active = active;
    this.expiration = moment().add(expiresIn, 'seconds').toJSON();
  }
}

class SessionService {
  /**
   * @description Initializes Redis with the given configuration.
   * @param {object} config Configuration used to establish a connection to Redis.
   * @param {object} Redis Redis dependency to be used bu the service.
   */
  async init(config, Redis = IoRedis) {
    this.redis = new Redis(config);
    const isReady = new Promise((res, rej) => {
      this.redis.on('connect', () => res());
      this.redis.on('error', err => rej(err));
    });
    return isReady;
  }

  /**
   * @description Retrieves a session that is associated with the provided token. The function
   * removes invalid sessions from the store and writes the array back to the store.
   * @param {string} token Base64 encoded key value pair in the format userId::sessionId.
   *
   * @returns {object} Returns an instance of the Session class.
   */
  async getSession(token) {
    const decodedToken = decodeToken(token);

    if (decodedToken === undefined) {
      return undefined;
    }

    const { key: userId, value: sessionId } = decodedToken;
    const namespace = SESSION_NAMESPACE(userId);
    const now = moment();
    const strSessions = await this.redis.get(namespace);

    if (strSessions === null) return undefined;

    const sessions = JSON.parse(strSessions);
    const validSessions = sessions.filter(session => now.diff(session.expiration) < 0);

    if (validSessions.length > 0) {
      await this.redis.set(namespace, JSON.stringify(validSessions));
    } else {
      await this.redis.del(namespace);
    }

    return validSessions.find(validSession => validSession.id === sessionId);
  }

  /**
   * @description Retrieves all valid sessions associated with the user. The function
   * removes invalid sessions from the store and writes the array back to the store.
   * @param {string} userId UUID that identifies the user.
   *
   * @returns {string} Returns an array of all the valid sessions associated with the user.
   */
  async getSessions(userId) {
    const namespace = SESSION_NAMESPACE(userId);
    const now = moment();
    const strSessions = await this.redis.get(namespace);
    const sessions = strSessions !== null ? JSON.parse(strSessions) : [];
    const validSessions = sessions.filter(session => now.diff(session.expiration) < 0);

    if (validSessions.length > 0) {
      await this.redis.set(namespace, JSON.stringify(validSessions));
    } else {
      await this.redis.del(namespace);
    }

    return validSessions;
  }

  /**
   * @description Makes the session, associated with the token, active. The function
   * removes invalid sessions from the store and writes the array back to the store.
   * @param {string} token Base64 encoded key value pair in the format userId::sessionId.
   *
   * @returns {object} Returns an instance of the Session class.
   */
  async makeActive(token) {
    const decodedToken = decodeToken(token);

    if (decodedToken === undefined) {
      return undefined;
    }

    const { key: userId, value: sessionId } = decodedToken;
    let updatedSession;
    const namespace = SESSION_NAMESPACE(userId);
    const now = moment();
    const sessions = await this.getSessions(userId);
    const validSessions = sessions.filter(session => now.diff(session.expiration) < 0);
    const updatedSessions = validSessions.map((session) => {
      if (session.id === sessionId) {
        updatedSession = Object.assign(session, { active: true });
        return updatedSession;
      }
      return Object.assign(session, { active: false });
    });

    if (updatedSessions.length > 0) {
      await this.redis.set(namespace, JSON.stringify(validSessions));
    } else {
      await this.redis.del(namespace);
    }
    return updatedSession;
  }

  /**
   * @description Creates a session (with the specified expiration) for the specified user.
   * The function removes invalid sessions from the store and writes the array back to the store.
   * @param {string} userId UUID that identifies the user.
   * @param {int} expiresIn Amount of seconds that the session is valid for.
   *
   * @returns {string} Returns an instance of the Session class.
   */
  async createSession(userId, expiresIn) {
    const sessionId = uuid.v4();
    const namespace = SESSION_NAMESPACE(userId);
    const newSession = new Session(sessionId, true, expiresIn);
    const sessions = await this.getSessions(userId);
    const updatedSessions = sessions.map(session => Object.assign(session, { active: false }));
    updatedSessions.push(newSession);
    await this.redis.set(namespace, JSON.stringify(updatedSessions), 'EX', expiresIn);

    return encodeToken(userId, sessionId);
  }

  /**
   * @description Deletes the session that is associated with the token. The function
   * removes invalid sessions from the store and writes the array back to the store.
   * @param {string} token Base64 encoded key value pair in the format userId::sessionId.
   *
   * @returns {object} Returns an instance of the Session class.
   */
  async deleteSession(token) {
    const decodedToken = decodeToken(token);

    if (decodedToken === undefined) {
      return undefined;
    }

    const { key: userId, value: sessionId } = decodedToken;
    const namespace = SESSION_NAMESPACE(userId);
    const sessions = await this.getSessions(userId);
    const deletedSession = sessions.find(session => session.id === sessionId);
    const validSessions = sessions.filter(session => session.id !== sessionId);
    const expirations = sessions.map(session => Math.abs(moment().diff(session.expiration, 'seconds')));
    const maxExpiresIn = Math.max(...expirations);

    if (validSessions.length > 0) {
      await this.redis.set(namespace, JSON.stringify(validSessions), 'EX', maxExpiresIn);
    } else {
      await this.redis.del(namespace);
    }

    return deletedSession;
  }
}

const sessionService = new SessionService();

export default sessionService;
