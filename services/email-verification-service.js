import IoRedis from 'ioredis';
import uuid from 'uuid';

import { encodeToken, decodeToken } from '../utils.js';

const EMAIL_VERIFICATION_NAMESPACE = userId => `users:${userId}:email:`;

class EmailVerificationService {
  /**
   * @description Initializes Redis with the given configuration.
   * @param {object} config Configuration used to establish a connection to Redis.
   * @param {object} Redis Redis dependency to be used bu the service.
   */
  async init(config, Redis = IoRedis) {
    this.expiresIn = config.expiresIn;
    this.redis = new Redis(config);
    const isReady = new Promise((res, rej) => {
      this.redis.on('connect', () => res());
      this.redis.on('error', err => rej(err));
    });
    return isReady;
  }

  /**
   * @description Creates an e-mail verification token to be sent to the user via e-mail.
   * @param {string} userId UUID that uniquely identifies the user the token
   * is being generated for.
   *
   * @returns {string} A base64 encoded token that identifies the verification request
   * (expires after the configured time).
   */
  async createToken(userId) {
    const namespace = EMAIL_VERIFICATION_NAMESPACE(userId);
    const tokenId = uuid.v4();
    const token = encodeToken(userId, tokenId);
    await this.redis.set(namespace, tokenId, 'EX', this.expiresIn);
    return token;
  }

  /**
   * @description Validates a verification request. If the token exists in the store
   * it is considered to be valid and th user can be verified.
   * @param {string} token Token that is to be validated.
   *
   * @returns {string} Returns the UUID associated with the user associated with the token.
   */
  async validateToken(token) {
    const decodedToken = decodeToken(token);

    if (decodedToken === undefined) {
      return undefined;
    }

    const { key: userId, value: tokenId } = decodedToken;
    const namespace = EMAIL_VERIFICATION_NAMESPACE(userId);
    const tokenIdInRedis = await this.redis.get(namespace);

    if (tokenIdInRedis === tokenId) {
      return userId;
    }

    return undefined;
  }

  /**
   * @description Removes a token from the store.
   * @param {string} token Token that is to be removed.
   *
   * @returns {string} Returns the UUID for the user associated with token.
   */
  async deleteToken(token) {
    const decodedToken = decodeToken(token);

    if (decodedToken === undefined) {
      return undefined;
    }

    const { key: userId } = decodedToken;
    const namespace = EMAIL_VERIFICATION_NAMESPACE(userId);
    const tokenIdInRedis = await this.redis.del(namespace);

    if (tokenIdInRedis > 0) {
      return userId;
    }

    return undefined;
  }
}

const emailVerificationService = new EmailVerificationService();

export default emailVerificationService;
