import axios from 'axios';
import { errors } from '@test/library-logging';


class EmailService {
  init({ location, apiVersion }) {
    this.baseUrl = `http://${location}/${apiVersion}`;
  }

  async sendVerificationEmail(email, token) {
    try {
      await axios.post(`${this.baseUrl}/verification`, { email, token });
    } catch (error) {
      throw errors.FailedDependency.logAndConvert(error, 'Failed to send verification e-mail.');
    }
  }

  // TODO: other e-mails to be added here
}

const emailService = new EmailService();

export default emailService;
