import config from 'config';

// import all your services
import sessionService from './session-service.js';
import emailService from './email-service.js';
import emailVerificationService from './email-verification-service.js';
import userService from './user-service.js';

export default async function initializeServices() {
  emailService.init(config.emailService);
  await sessionService.init(config.sessionService);
  await emailVerificationService.init(config.emailVerificationService);
  await userService.init(config.userService);
}
