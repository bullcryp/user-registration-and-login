import config from 'config';
import Redis from 'ioredis-mock';
import uuid from 'uuid';

import emailVerificationService from '../../services/email-verification-service.js';

describe('Email Verification Service Tests', () => {
  beforeAll(async () => {
    await emailVerificationService.init(config.sessionService, Redis);
  });

  test('Create a token for a user.', async () => {
    const userId = uuid.v4();
    const createdToken = await emailVerificationService.createToken(userId);
    let retrievedUserId = await emailVerificationService.validateToken(createdToken);
    expect(retrievedUserId).toBe(userId);
    const deletedUserId = await emailVerificationService.deleteToken(createdToken);
    retrievedUserId = await emailVerificationService.validateToken(createdToken);
    expect(retrievedUserId).not.toBe(deletedUserId);
    expect(retrievedUserId).toBe(undefined);
  });

  test('Token expiration.', async () => {
    const userId = uuid.v4();
    const createdToken = await emailVerificationService.createToken(userId);
    setTimeout(async () => {
      const retrievedUserId = await emailVerificationService.validateToken(createdToken);
      expect(retrievedUserId).toBe(undefined);
    }, 1000);
  });

  test('Attempt to validate an invalid token.', async () => {
    const result = await emailVerificationService.deleteToken('invalidToken');
    expect(result).toBe(undefined);
  });

  test('Attempt to delete invalid token.', async () => {
    const result = await emailVerificationService.deleteToken('invalidToken');
    expect(result).toBe(undefined);
  });

  test('Attempt to delete valid token that does not exist.', async () => {
    const result = await emailVerificationService.deleteToken('userId::sessionId');
    expect(result).toBe(undefined);
  });
});
