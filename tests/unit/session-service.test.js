import config from 'config';
import uuid from 'uuid';
import Redis from 'ioredis-mock';

import sessionService from '../../services/session-service.js';

describe('Session Service Tests', () => {
  beforeAll(async () => {
    await sessionService.init(config.sessionService, Redis);
  });

  test('Create a session for a user.', async () => {
    const userId = uuid.v4();
    const token = await sessionService.createSession(userId, 1000);
    const retrievedSession = await sessionService.getSession(token);
    expect(retrievedSession.active).toBe(true);
  });

  test('Create multiple sessions for a user and take control.', async () => {
    const userId = uuid.v4();
    const firstToken = await sessionService.createSession(userId, 1000);
    const secondToken = await sessionService.createSession(userId, 1000);
    await sessionService.makeActive(firstToken);
    const firstCreatedSessionAfterUpdate = await sessionService.getSession(firstToken);
    const secondCreatedSessionAfterUpdate = await sessionService.getSession(secondToken);
    expect(firstCreatedSessionAfterUpdate.active).toBe(true);
    expect(secondCreatedSessionAfterUpdate.active).toBe(false);
  });

  test('Create multiple sessions for a user.', async () => {
    const userId = uuid.v4();
    for (let k = 0; k < 100; k += 1) {
      // eslint-disable-next-line no-await-in-loop
      const token = await sessionService.createSession(userId, 1000);
      // eslint-disable-next-line no-await-in-loop
      const currentSession = await sessionService.getSession(token);
      // eslint-disable-next-line no-await-in-loop
      const allSessions = await sessionService.getSessions(userId);

      for (let l = 0; l < allSessions.length; l += 1) {
        if (allSessions[l].id !== currentSession.id) {
          expect(allSessions[l].active).toBe(false);
        }
      }
    }
  });

  test('Test session expirations.', async () => {
    const userId = uuid.v4();
    const token = await sessionService.createSession(userId, 0);

    setTimeout(async () => {
      const retrievedSession = await sessionService.getSession(token);
      expect(retrievedSession).toBe(undefined);
    }, 100);
  });

  test('Delete a session for a user.', async () => {
    const userId = uuid.v4();
    const tokens = [];

    for (let k = 0; k < 100; k += 1) {
      // eslint-disable-next-line no-await-in-loop
      tokens.push(await sessionService.createSession(userId, 1000));
    }

    for (let k = 0; k < tokens.length; k += 1) {
      // eslint-disable-next-line no-await-in-loop
      await sessionService.deleteSession(tokens[k]);
    }
    const allSessions = await sessionService.getSessions(userId);
    expect(allSessions.length).toBe(0);
  });
});
