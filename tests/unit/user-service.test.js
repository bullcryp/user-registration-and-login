import config from 'config';
import { errors } from '@test/library-logging';

import userService from '../../services/user-service.js';

describe('User Service Tests', () => {
  let testNumber = 1;

  beforeAll(async () => {
    await userService.init(config.userService);
  });
  beforeEach(() => {
    testNumber += 1;
  });

  test('Create a user in the unverified state', async () => {
    const user = await userService.createUser(
      `test${testNumber}@test.com`,
      'thisisapassword',
      true,
    );
    expect(user).not.toBeUndefined();
  });

  test('Fail to create a user because it already exists', async () => {
    const user = await userService.createUser(
      `test${testNumber}@test.com`,
      'thisisapassword',
      true,
    );
    expect(user).not.toBeUndefined();
    try {
      await userService.createUser(
        `test${testNumber}@test.com`,
        'thisisapassword',
        true,
      );
    } catch (error) {
      expect(error).toBeInstanceOf(errors.Conflict);
    }
  });

  test('Create and verify a user', async () => {
    const user = await userService.createUser(
      `test${testNumber}@test.com`,
      'thisisapassword',
      true,
    );
    expect(user).not.toBeUndefined();
    const newlyVerifiedUser = await userService.verifyUser(user.userId);
    expect(newlyVerifiedUser).not.toBeUndefined();
    const reverifiedUser = await userService.verifyUser(user.userId);
    expect(reverifiedUser).not.toBeUndefined();
  });

  test('Attempt to verify a non-existent user', async () => {
    try {
      await userService.verifyUser('notavaliduuid');
    } catch (error) {
      expect(error).toBeInstanceOf(errors.NotFound);
    }
  });

  test('Find an unverified user by ID', async () => {
    const user = await userService.createUser(
      `test${testNumber}@test.com`,
      'thisisapassword',
      true,
    );
    expect(user).not.toBeUndefined();
    const foundUser = await userService.findUserById(user.userId);
    expect(foundUser).not.toBeUndefined();
  });

  test('Find a verified user by ID', async () => {
    const user = await userService.createUser(
      `test${testNumber}@test.com`,
      'thisisapassword',
      true,
    );
    expect(user).not.toBeUndefined();
    const newlyVerifiedUser = await userService.verifyUser(user.userId);
    expect(newlyVerifiedUser).not.toBeUndefined();
    const foundUser = await userService.findUserById(user.userId);
    expect(foundUser).not.toBeUndefined();
  });

  test('Fail to find a user by ID', async () => {
    const foundUser = await userService.findUserById('notavaliduuid');
    expect(foundUser).toBeNull();
  });

  test('Find an unverified user by e-mail', async () => {
    const user = await userService.createUser(
      `test${testNumber}@test.com`,
      'thisisapassword',
      true,
    );
    expect(user).not.toBeUndefined();
    const foundUser = await userService.findUserByEmail(user.email);
    expect(foundUser).not.toBeUndefined();
  });

  test('Find a verified user by e-mail', async () => {
    const user = await userService.createUser(
      `test${testNumber}@test.com`,
      'thisisapassword',
      true,
    );
    expect(user).not.toBeUndefined();
    const newlyVerifiedUser = await userService.verifyUser(user.userId);
    expect(newlyVerifiedUser).not.toBeUndefined();
    const foundUser = await userService.findUserByEmail(user.email);
    expect(foundUser).not.toBeUndefined();
  });

  test('Fail to find a user by e-mail', async () => {
    const foundUser = await userService.findUserByEmail('notavalidemail');
    expect(foundUser).toBeNull();
  });
});
