import uuid from 'uuid';

import { encodeToken, decodeToken } from '../../utils.js';

describe('Utils Tests', () => {
  test('Encode and decode a token.', async () => {
    const key = uuid.v4();
    const value = uuid.v4();
    const encodedToken = encodeToken(key, value);
    const decodedToken = decodeToken(encodedToken);
    expect(decodedToken.key).toBe(key);
    expect(decodedToken.value).toBe(value);
  });

  test('Attempt to decode an invalid token.', async () => {
    const decodedToken = decodeToken('notavalidtoken');
    expect(decodedToken).toBe(undefined);
  });
});
