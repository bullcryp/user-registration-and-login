import axios from 'axios';
import request from 'supertest';
import sinon from 'sinon';
import httpStatusCodes from 'http-status-codes';
import Redis from 'ioredis-mock';
import config from 'config';

import app from '../../index.js';
import sessionService from '../../services/session-service.js';
import emailService from '../../services/email-service.js';
import emailVerificationService from '../../services/email-verification-service.js';
import userService from '../../services/user-service.js';


const initializeServices = async () => {
  emailService.init(config.emailService);
  await sessionService.init(config.sessionService, Redis);
  await emailVerificationService.init(config.emailVerificationService, Redis);
  await userService.init(config.userService);
};

describe('User registration', () => {
  let server;
  let sandbox;
  let agent;

  beforeAll(async (done) => {
    await initializeServices();
    server = app.listen(4000, (err) => {
      if (err) return done(err);
      agent = request.agent(server);
      return done();
    });
  });
  afterAll(done => server && server.close(done));
  beforeEach(() => {
    sandbox = sinon.createSandbox();
    sandbox.stub(emailService, 'sendVerificationEmail').returns(new Promise(res => res()));
  });
  afterEach(() => sandbox.restore());

  test('Succesfully register a user', async () => {
    const body = {
      email: 'test@test.com', password: 'password', termsAccepted: true, marketingAccepted: false,
    };
    const res = await agent.post('/v1/user/register').send(body);
    expect(res.status).toBe(httpStatusCodes.OK);
  });

  test('Fail to register user due to invalid e-mail', async () => {
    sandbox.stub(axios, 'post').returns(new Promise(res => res())); // stub out e-mail service request even though it should never hit it
    const body = {
      email: 'notavalidemail', password: 'password', termsAccepted: true, marketingAccepted: false,
    };
    const res = await agent.post('/v1/user/register').send(body);
    expect(res.status).toBe(httpStatusCodes.BAD_REQUEST);
  });

  test('Fail to register user due to password being to be short', async () => {
    sandbox.stub(axios, 'post').returns(new Promise(res => res())); // stub out e-mail service request even though it should never hit it
    const body = {
      email: 'notavalidemail', password: 'sh', termsAccepted: true, marketingAccepted: false,
    };
    const res = await agent.post('/v1/user/register').send(body);
    expect(res.status).toBe(httpStatusCodes.BAD_REQUEST);
  });

  test('User already exists', async () => {
    sandbox.stub(axios, 'post').returns(new Promise(res => res())); // stub out e-mail service request even though it should never hit it
    const body = { email: 'test@test.com', password: 'password', termsAccepted: true };
    const res = await agent.post('/v1/user/register').send(body);
    expect(res.status).toBe(httpStatusCodes.CONFLICT);
  });

  test('Register user with persistent login', async () => {
    sandbox.stub(axios, 'post').returns(new Promise(res => res())); // stub out e-mail service request even though it should never hit it
    const body = {
      email: 'different-test@test.com', password: 'password', rememberMe: true, termsAccepted: true,
    };
    const res = await agent.post('/v1/user/register').send(body);
    expect(res.status).toBe(httpStatusCodes.OK);
  });
});

describe('User session management', () => {
  let server;
  let sandbox;
  let agent;
  const existingUser = {
    email: 'test@test.com', password: 'password', rememberMe: true, termsAccepted: true, marketingAccepted: false,
  };

  beforeAll(async (done) => {
    await initializeServices();
    server = app.listen(4000, async (err) => {
      if (err) return done(err);
      agent = request.agent(server);

      sandbox = sinon.createSandbox();
      sandbox.stub(emailService, 'sendVerificationEmail').returns(new Promise(res => res()));
      const body = existingUser;
      const registration = await request(server).post('/v1/user/register').send(body); // create user before we attempt to login
      existingUser.userId = registration.body.userId;
      existingUser.token = registration.body.token;
      expect(registration.status).toBe(httpStatusCodes.OK);
      sandbox = sandbox.restore();

      return done();
    });
  });
  afterAll(done => server && server.close(done));
  beforeEach(() => {
    sandbox = sinon.createSandbox();
  });
  afterEach(() => sandbox.restore());

  test('Sucessfull login', async () => {
    const body = { email: existingUser.email, password: existingUser.password };
    const result = await agent.post('/v1/user/session').send(body);
    expect(result.status).toBe(httpStatusCodes.OK);
  });

  test('Incorrect password', async () => {
    const body = { email: existingUser.email, password: 'notmypassword' };
    const result = await agent.post('/v1/user/session').send(body);
    expect(result.status).toBe(httpStatusCodes.UNAUTHORIZED);
  });

  test('Incorrect e-mail', async () => {
    const body = { email: 'doesnotexist@test.com', password: 'doesnotmatter', rememberMe: true };
    const result = await agent.post('/v1/user/session').send(body);
    expect(result.status).toBe(httpStatusCodes.UNAUTHORIZED);
  });

  test('Retrieve a valid session', async () => {
    const result = await agent.get(`/v1/user/session/${existingUser.token}`);
    expect(result.status).toBe(httpStatusCodes.OK);
  });

  test('Attempt to retrieve an invalid session', async () => {
    const result = await agent.get('/v1/user/session/somerandomtoken');
    expect(result.status).toBe(httpStatusCodes.UNAUTHORIZED);
  });

  test('Make a sessions the active session', async () => {
    const body = { email: existingUser.email, password: existingUser.password };
    const loginResult = await agent.post('/v1/user/session').send(body);
    expect(loginResult.status).toBe(httpStatusCodes.OK);
    const activationResult = await agent.patch('/v1/user/active-session').send({ token: existingUser.token });
    expect(activationResult.status).toBe(httpStatusCodes.OK);
  });

  test('Attempt to make an invalid session active', async () => {
    const result = await agent.patch('/v1/user/active-session').send({ token: 'someinvalidtoken' });
    expect(result.status).toBe(httpStatusCodes.BAD_REQUEST);
  });

  test('Delete a session', async () => {
    const body = { email: existingUser.email, password: existingUser.password };
    const loginResult = await agent.post('/v1/user/session').send(body);
    expect(loginResult.status).toBe(httpStatusCodes.OK);
    const deletionResult = await agent.delete('/v1/user/session').send({ token: loginResult.body.token });
    expect(deletionResult.status).toBe(httpStatusCodes.OK);
  });

  test('Attempt to delete an invalid session', async () => {
    const result = await agent.delete('/v1/user/session').send({ token: 'someinvalidtoken' });
    expect(result.status).toBe(httpStatusCodes.BAD_REQUEST);
  });
});

describe('User verification', () => {
  let server;
  let sandbox;
  let agent;
  const existingUser = {
    email: 'test@test.com', password: 'password', rememberMe: true, termsAccepted: true,
  };

  beforeAll(async (done) => {
    await initializeServices();
    server = app.listen(4000, async (err) => {
      if (err) return done(err);
      agent = request.agent(server);

      sandbox = sinon.createSandbox();
      sandbox.stub(emailService, 'sendVerificationEmail').returns(new Promise(res => res()));
      const body = existingUser;
      const registration = await request(server).post('/v1/user/register').send(body); // create user before we attempt to login
      existingUser.userId = registration.body.userId;
      expect(registration.status).toBe(httpStatusCodes.OK);
      sandbox = sandbox.restore();

      return done();
    });
  });
  afterAll(() => {
    server.close();
  });
  beforeEach(() => {
    sandbox = sinon.createSandbox();
  });
  afterEach(() => sandbox.restore());

  test('Successfully verify user.', async () => {
    sandbox.stub(emailVerificationService, 'validateToken').returns(existingUser.userId);
    sandbox.stub(emailVerificationService, 'deleteToken').returns(existingUser.userId);
    const body = { token: 'sometoken' };
    const result = await agent.patch('/v1/user/verification-status').send(body);
    expect(result.status).toBe(httpStatusCodes.OK);
  });

  test('Fail to verify user due to missing verification request.', async () => {
    const body = { token: 'sometoken' };
    const result = await agent.patch('/v1/user/verification-status').send(body);
    expect(result.status).toBe(httpStatusCodes.NOT_FOUND);
  });
});
