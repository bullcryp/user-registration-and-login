import { errors } from '@test/library-logging';

import { SERVICE_NAME, SERVICE_VERSION } from '../consts.js';

function error(err, req, res, next) {
  // call the default error handler in Express in case
  // an error occurred after the the response was sent
  if (res.headerSent) {
    return next(err);
  }

  // We expect the array to only exist if it is Swagger Validation error.
  if (Array.isArray(err.errors)) {
    const result = {
      key: 'ValidationError',
      errors: err.errors,
    };
    return res.status(err.status).send(result);
  }

  if (!(err instanceof errors.GenericError)) {
    const internalServerError = errors.InternalServerError.logAndConvert(err, `An unexpected error occurred in ${SERVICE_NAME} ${SERVICE_VERSION}`);
    return res.status(internalServerError.statusCode).send(internalServerError.serialize());
  }

  return res.status(err.statusCode).send(err.serialize());
}

export default error;
