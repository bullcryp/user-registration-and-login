import uuid from 'uuid';
import microtime from 'microtime';

import { logger } from '@test/library-logging';

export function logRequest(req, res, next) {
  req.locals = { id: uuid.v4(), receivedTime: microtime.nowDouble() };
  logger.info('Logged request.', { reqId: req.locals.id, headers: req.headers, body: req.body });
  next();
}

export function logResponse(req, res, next) {
  const originalSend = res.send;

  // The implementation of res.send results in it being called
  // multiple times to do serialization of objects. The alreadyLogged
  // flag stops us from logging each intermediate step.
  let alreadyLogged = false;

  res.send = (data) => {
    if (res.headersSent) {
      return res;
    }

    if (!alreadyLogged) {
      // Workaround: res.getHeaders() returns an object without a
      // prototype so the logger can't stringify it.
      const headers = { ...res.getHeaders() };
      const processingTime = microtime.nowDouble() - req.locals.receivedTime;
      logger.info('Logged response.', {
        reqId: req.locals.id,
        headers,
        body: data,
        processingTime,
      });
      alreadyLogged = true;
    }

    return originalSend.apply(res, [data]);
  };

  next();
}

export default {
  logRequest,
  logResponse,
};
