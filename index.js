import express from 'express';
import swaggerUi from 'swagger-ui-express';
import yml2json from 'yamljs';
import path from 'path';
import * as oas from 'express-openapi-validator';
import { logger, errors } from '@test/library-logging';

import user from './routes/v1/users.js';
import health from './routes/v1/health.js';
import error from './middleware/error.js';
import notFound from './middleware/not-found.js';
import { logRequest, logResponse } from './middleware/logging.js';
import initializeServices from './services/initialize.js';

// Import all Swagger YAML here.
const apiDocV1 = yml2json.load('./routes/v1/api-doc.yaml');

// Initialize Express application.
const app = express();

// Keep this first because the order of middleware is important.
app.use(express.json());
app.use(logRequest);
app.use(logResponse);

// Deploy express validator and point to OPEN API 3.
const source = path.join(process.cwd(), '/routes/v1/api-doc.yaml');
new (oas.default || oas).OpenApiValidator({
  apiSpec: source,
}).install(app);

// Add service middleware here and remember the order is important.
app.use('/v1/health', health);
app.use('/v1/user', user);

// Serves the Swagger UI documentation on the route.
app.use('/v1/api-doc', swaggerUi.serve, swaggerUi.setup(apiDocV1));

// Keep this last because the order of middleware is important.
app.use(notFound);
app.use(error);

if (process.env.NODE_ENV !== 'test') {
  initializeServices().then(() => {
    const port = 3000;
    app.listen(port, () => logger.info(`Listening on port ${port}...`));
  }).catch((err) => {
    errors.GenericError.logAndConvert(err, 'Failed to initialize services. Application will shutdown.');
    process.exit();
  });
}

export default app;
